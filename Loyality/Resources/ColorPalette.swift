//
//  ColorPalette.swift
//  Loyality
//
//  Created by Димон on 22.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

internal enum ColorPalette {
    
    static let clearColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    
    static let whiteColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    static let commonGrayColor = #colorLiteral(red: 0.3999590278, green: 0.4000294805, blue: 0.3999440968, alpha: 1)
    
    static let dateColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    static let commonTextColor = #colorLiteral(red: 0.3999590278, green: 0.4000294805, blue: 0.3999440968, alpha: 1)
    
    static let trainingColor = #colorLiteral(red: 0.9908211827, green: 0.8018125892, blue: 0.03470525518, alpha: 1)
    
    static let messangerColor = #colorLiteral(red: 0.001006016973, green: 0.5922899246, blue: 0.8170413375, alpha: 1)
    
    static let homeColor = #colorLiteral(red: 0.2774619758, green: 0.6093608737, blue: 0.1735032797, alpha: 1)
    
    static let balanceColor = #colorLiteral(red: 0.005005622748, green: 0.2784627974, blue: 0.5693361163, alpha: 1)

}

