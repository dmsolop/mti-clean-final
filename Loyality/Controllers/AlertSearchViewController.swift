//
//  AlertViewController.swift
//  Loyality
//
//  Created by Димон on 14.04.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class AlertSearchViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView! {
        didSet {
            alertView.layer.cornerRadius = 50 / 2
            alertView.layer.borderWidth = 2
            alertView.layer.borderColor = UIColor.newDesignYellow.cgColor
            alertView.layer.shadowColor = UIColor.black.cgColor
            alertView.layer.shadowOffset = CGSize(width: 2, height: 2)
            alertView.layer.shadowOpacity = Float(0.6)
            alertView.layer.shadowRadius = 4
            alertView.layer.backgroundColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var customSearchBar: UISearchBar! {
        didSet {
            customSearchBar.showsSearchResultsButton = true
            customSearchBar.showsCancelButton = true
            customSearchBar.compatibleSearchTextField.textColor = UIColor.darkGray
            customSearchBar.compatibleSearchTextField.backgroundColor = UIColor.white
            customSearchBar.compatibleSearchTextField.rightView = UIImageView(image: UIImage(named: "search_white"))
            customSearchBar.compatibleSearchTextField.rightViewMode = UITextField.ViewMode.always
            let image = UIImage(named: "search_white")?.resizeImage(CGFloat(25), opaque: false)
            customSearchBar.setImage(image, for: .search, state: .normal)
            customSearchBar.setPositionAdjustment(UIOffset(horizontal: -5, vertical: 0), for: .search)
            customSearchBar.layoutIfNeeded()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    //MARK: - Routing
    static func storyboardInstance() -> AlertSearchViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: AlertSearchViewController.self)) as? AlertSearchViewController
    }


}
