//
//  WebViewController.swift
//  Loyality
//
//  Created by Димон on 02.04.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate{
    
    var webView: WKWebView!
    
    let stringUrl: String
    let userId: String
    let taskId: String
    
    init(stringUrl: String, userId: String, taskId: String) {
        self.stringUrl = stringUrl
        self.userId = userId
        self.taskId = taskId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.allowsBackForwardNavigationGestures = true
        let paramsString = "{\"USER_ID\": \(userId), \"TASK_ID\": \(taskId)}"
        let paramsBytes: [UInt8] = Array(paramsString.utf8)
        let paramsBase64 = paramsBytes.toBase64()
        let params = ["q": paramsBase64]
        let postString = self.getPostString(params: params as! [String : String])
        if let url = URL(string: stringUrl + "?" + postString) {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "GET"
            webView.load(urlRequest)
        }
    }
    
    func getPostString(params:[String:String]) -> String {
        var data = [String]()
        for (key, value) in params {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
}
