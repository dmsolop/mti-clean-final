//
//  LoginViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 1/5/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class VerificationViewController: BaseViewController {

    @IBOutlet weak var codeTextField: JVFloatLabeledTextField!
    @IBOutlet weak var stackView: UIStackView!
    
    var pin = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if isRegistration {
//            stackView.subviews.last?.removeFromSuperview()
//        } else {
//
//        }
    }
    
    @IBAction func registrationButtonPressed(_ sender: Any) {
        guard let sms = codeTextField.text else { return }
        Communicator.registration(phone: Session.phone!, sms: sms, name: Session.name!, email: Session.mail!) { (response) in
            
            if let status = response["STATUS"],  status as! String == "OK" {
                Communicator.login(phone: Session.phone!) { [weak self] (response) in
                    Session.pin = self?.pin
                    Threads.performTaskInMainQueue {
                        self?.performSegue(withIdentifier: "showTabbarController", sender: self)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Введен неверный код!", message: "Убедитесь в правильности вводимых символов либо повторите процесс регистрации", preferredStyle: .alert)
                let actionConfirm = UIAlertAction(title: "OK", style: .cancel, handler: {_ in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(actionConfirm)
                self.present(alert, animated: true, completion: nil)
            }
        }
        

        
//        Security.encrypt(type: "", id: codeTextField.text!, publicKey: "")
        
        
    }
    
}
