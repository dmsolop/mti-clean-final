//
//  MainViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 1/5/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var listButton: UIButton!
    @IBOutlet private weak var gridButton: UIButton!
    @IBOutlet private weak var balanceLabel: UILabel!
    @IBOutlet private weak var menuBtn: UIButton!
    
    private var viewStyle: PartnerViewStyle = .tileStyle
    private var pins = [MapObject]()
    private var partners = [Partner]()
    private var allPartners = [Partner]()
    private var isListButton = false
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        initializingCollectionView()
        balanceLabel.text = "₴ \(Session.balance)"
        Communicator.map(phone: Session.phone!) { [weak self] (response) in
            print(response)
            self?.pins = response
        }
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Идет обновление...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        collectionView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        Communicator.partner(phone: Session.phone!) { [weak self] (response) in
            self?.allPartners = response
            self?.partners = response
            self?.collectionView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailController" {
            (segue.destination as! DetailViewController).partner = sender as? Partner
            (segue.destination as! DetailViewController).pins = pins
        }
    }
    
    
    @IBAction func styleButtonDidPressed(sender: UIButton) {
        sender.tintColor = UIColor.lightBlue
        sender.borderColor = UIColor.lightBlue
        switch sender.tag {
        case 0:
            isListButton = true
            viewStyle = .tableStyle
            gridButton.tintColor = UIColor.simpleGray
            gridButton.borderColor = UIColor.simpleGray
        case 1:
            isListButton = false
            viewStyle = .tileStyle
            listButton.tintColor = UIColor.simpleGray
            listButton.borderColor = UIColor.simpleGray
        default:
            break
        }
        
        collectionView.reloadData()
    }
    
    @IBAction func pushedMenuBtn(_ sender: UIButton) {
        if let popUPMenuVC = PopUpMenuVC.storyboardInstance() {
            popUPMenuVC.modalTransitionStyle = .coverVertical
            popUPMenuVC.modalPresentationStyle = .overCurrentContext
            popUPMenuVC.delegate = self
            self.present(popUPMenuVC, animated: true, completion: nil)
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshBegin(newtext: "Refresh",
                     refreshEnd: {(x:Int) -> () in
                        self.collectionView.reloadData()
                        self.refreshControl.endRefreshing()
        })
    }
    
    func refreshBegin(newtext:String, refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(qos: .default).async {
            print("refreshing")
            
            Communicator.partner(phone: Session.phone!) { [weak self] (response) in
                self?.allPartners = response
                self?.partners = response
                self?.collectionView.reloadData()
            }
            sleep(2)
            DispatchQueue.main.async {
                self.balanceLabel.text = "₴ \(Session.balance)"
                refreshEnd(0)
            }
        }
    }
    
    
    private func initializingCollectionView() {
        if let layout = collectionView?.collectionViewLayout as? PartnerCellLayout {
            layout.delegate = self
        }
        let identifier = String(describing: PartnerCollectionViewCell.self)
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

//MARK: - UISearchBarDelegate
extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            partners = allPartners
        } else {
            let searchPartners = allPartners.filter{($0.name.contains(searchText))}
            partners = searchPartners
        }
        collectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

//MARK: - UICollectionViewDelegate
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, PartnerCellLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let partner = partners[indexPath.row]
        let urlLogo = isListButton ? partner.logoBig : partner.logo
        
        let identifier = String(describing: PartnerCollectionViewCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! PartnerCollectionViewCell
        cell.balanceTitle.text = "\(partner.balance)"
        cell.logoImageView.sd_setImage(with: URL(string: urlLogo), completed: nil)
        cell.circleImageView.sd_setImage(with: URL(string: partner.circleLogo), completed: nil)
        cell.backgroundColor = UIColor.clear
        cell.logoBackgroundView.backgroundColor = partner.color()
        cell.circleView.backgroundColor = partner.circleColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let partner = partners[indexPath.row]
        perform(segue: StoryboardSegue.Main.showDetailController, sender: partner)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return partners.count
    }
    
    func collectionView(_ collectionView: UICollectionView) -> PartnerViewStyle {
        return viewStyle
    }
}

extension MainViewController: PopUpMenuDelegate {
    func selectTabBarItem(index: Int) {
        tabBarController!.selectedIndex = index
    }
    
    func sendingCurrentViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
