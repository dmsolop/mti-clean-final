//
//  MoneyTransferVC.swift
//  Loyality
//
//  Created by Димон on 06.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class MoneyTransferVC: BaseViewController {

    
    @IBOutlet weak var pepsellImage: UIImageView!
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var emptyLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var userPayments = [UserPayment]()
    
    var refreshControl:UIRefreshControl!
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        balanceLabel.text = "₴ \(Session.balance)"
        setUpRefreshControl()
        getPayments()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - Routing
    static func storyboardInstance() -> MoneyTransferVC? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: MoneyTransferVC.self)) as? MoneyTransferVC
    }
    
    //MARK: - Refreshing
    fileprivate func getPayments() {
        showLoader()
         Communicator.userPayments(phone: Session.phone!) { [unowned self] (response) in
            self.hideLoader()
            self.userPayments = response
            if self.userPayments.isEmpty {
                self.emptyLabel.isHidden = false
                self.emptyLabel.text = "У вас пока ещё нет зачислений"
            } else {
                self.emptyLabel.isHidden = true
                self.tableView.reloadData()
            }
         }
     }
    
    private func setUpRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Идет обновление...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshBegin(newtext: "Refresh",
                     refreshEnd: {(x:Int) -> () in
                        self.refreshControl.endRefreshing()
        })
    }
    
    func refreshBegin(newtext:String, refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(qos: .default).async {
            print("refreshing")
            self.getPayments()
            sleep(2)
            DispatchQueue.main.async {
                self.balanceLabel.text = "₴ \(Session.balance)"
                refreshEnd(0)
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func menuBtnClicked(_ sender: UIButton) {
        if let popUPMenuVC = PopUpMenuVC.storyboardInstance() {
            popUPMenuVC.modalTransitionStyle = .coverVertical
            popUPMenuVC.modalPresentationStyle = .overCurrentContext
            popUPMenuVC.delegate = self
            self.present(popUPMenuVC, animated: true, completion: nil)
        }
    }
    
}

//MARK: - Table view delegate
extension MoneyTransferVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userPayments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MoneyTransferCell.self), for: indexPath) as! MoneyTransferCell
        let payment = self.userPayments[indexPath.row]
                
        return setupCells(cell: cell, payment: payment)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in userPayments.indices {
            userPayments[i].isShowed = i == indexPath.row ? !userPayments[i].isShowed : false
        }
        tableView.reloadData()
    }
    
    private func setupCells(cell: MoneyTransferCell, payment: UserPayment) ->  MoneyTransferCell {
        
        let imageViewHeight: CGFloat = 200
        let imageViewTop: CGFloat = 4
        let imageViewBottom: CGFloat = 8
        var commonSubviewHeight: CGFloat = 122
                
        cell.codecIDLabel.text = payment.codecID
        cell.payDateLabel.text = payment.payDate
        let amount = String(payment.amount)
        cell.amountLabel.text = "+" + amount + " грн"
                
        if let answer = payment.answer1, answer != "" {
            cell.answer1Label.text = "Відповідь: " + answer
        } else {
            cell.answer1Label.text = ""
        }
        
        if payment.messageText.first == "<" {
            cell.textView.attributedText = payment.messageText.convertHtml()
        } else {
            cell.textView.text = payment.messageText
        }
        
        if let answer2 = payment.answer2 {
            if answer2.contains("http") {
                cell.answer2Label.text = ""
                cell.answer2ImageView.sd_setImage(with: URL(string: answer2)) { (image, error, casheImage, url) in
                    if let err = error {
                        print(err)
                        cell.answer2ImageView.isHidden = true
                        cell.imageViewHeight.constant = 0
                        cell.imegeViewTop.constant = 0
                        cell.imageViewBottom.constant = 0
                    } else {
                        cell.imageViewHeight.constant = imageViewHeight
                        cell.imegeViewTop.constant = imageViewTop
                        cell.imageViewBottom.constant = imageViewBottom
                        cell.answer2ImageView.isHidden = false
                        commonSubviewHeight += (imageViewHeight + imageViewTop + imageViewBottom)
                    }
                }
            } else {
                cell.answer2Label.text = "Зіскановано: " + answer2
                cell.imageViewHeight.constant = 0
                cell.imegeViewTop.constant = 0
                cell.imageViewBottom.constant = 0
            }
        } else {
            cell.answer2Label.text = ""
            cell.imageViewHeight.constant = 0
            cell.imegeViewTop.constant = 0
            cell.imageViewBottom.constant = 0
        }
        
        cell.subViewHeight.constant = payment.isShowed ? commonSubviewHeight : 0
        
        return cell
    }
    
}

//MARK: - PopUpMenu Delegate
extension MoneyTransferVC: PopUpMenuDelegate {
    func selectTabBarItem(index: Int) {
        tabBarController!.selectedIndex = index
    }
    
    func sendingCurrentViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
