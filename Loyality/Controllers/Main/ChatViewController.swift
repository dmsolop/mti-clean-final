//
//  ChatViewController.swift
//  Loyality
//
//  Created by Dmytro Solop on 7/4/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class ChatViewController: BaseViewController {
    
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var balanceLabel: UILabel!
    
    var pickImageCallback: ((UIImage) -> ())?
    var getTextCallback: ((String, String, UIViewController) -> ())?
    var localeBtn: JJActionItem = JJActionItem()
    var getBottomBtn: JJActionItem = JJActionItem()
    
    private var isSearchVisible = false
    private var isActionsDone = false
    private var isFirst = true
    private var searchString = ""
    private var allMessages = [ChatEntry]() {
        didSet{
            messages = allMessages
        }
    }
    private var selectedMessages = [ChatEntry]() {
        didSet{
            messages = selectedMessages
        }
    }
    private var combinedMessages = [ChatEntry]() {
        didSet{
            messages = combinedMessages
        }
    }
    private var messages = [ChatEntry]() {
        didSet {
            if isFirst {
                tableView.reloadData()
                isFirst = false
            }
        }
    }
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        balanceLabel.text = "₴ \(Session.balance)"
        setJJFloatingActionButton()
        Session.indexPath = nil
        chatHistory(){}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Notifications

    private func postNotificationIsActions() {
        NotificationCenter.default.post(.init(name: .isActionsDone, object: nil, userInfo: ["isActions":isActionsDone]))
    }
    
    //MARK: - Actions
        @IBAction func menuBtnClicked(_ sender: Any) {
        if let popUPMenuVC = PopUpMenuVC.storyboardInstance() {
            popUPMenuVC.modalTransitionStyle = .coverVertical
            popUPMenuVC.modalPresentationStyle = .overCurrentContext
            popUPMenuVC.delegate = self
            self.present(popUPMenuVC, animated: true, completion: nil)
        }
    }
    
    //MARK: - Show filtering messages view
    private func showFilteringMessagesView() {
        if let alertSearchVC = AlertSearchViewController.storyboardInstance() {
            alertSearchVC.view.backgroundColor = UIColor.clear
            if let searchBar = alertSearchVC.customSearchBar {
                searchBar.delegate = self
            }
            if let alertView = alertSearchVC.alertView, !isSearchVisible {
                alertView.tag = 100
                self.view.addSubview(alertView)
                
                alertView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 67).isActive = true
                alertView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
                
                NSLayoutConstraint(item: alertView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.8, constant: 0).isActive = true
                view.layoutIfNeeded()
                isSearchVisible = true
            } else if isSearchVisible {
                removeSubview(tag: 100)
                messages = allMessages
                tableView.reloadData()
            }
        }
    }
        
    func removeSubview(tag: Int){
            print("Start subview removing")
            if let viewWithTag = self.view.viewWithTag(tag) {
                viewWithTag.removeFromSuperview()
                isSearchVisible = false
                view.endEditing(isSearchVisible)
            }else{
                print("No one compatible view for removing from superview")
            }
        }
    
    //MARK: - Requests
    func chatHistory(withDelay delay: UInt32 = 0, completion: @escaping ()->()) {
        showLoader()
        sleep(delay)
        Communicator.chatHistory(phone: Session.phone!) { [unowned self] (response) in
            
            if self.isSearchVisible, self.searchString != "" {
                let tempArr = response
                let searchMessages = tempArr.filter{($0.text.lowercased().contains(self.searchString.lowercased()))}
                self.selectedMessages = self.displayShowedMessages(arr1: self.messages, arr2: searchMessages)
            } else {
                self.allMessages = self.displayShowedMessages(arr1: self.messages, arr2: response)

            }
            if let indexPath = Session.indexPath {
                self.openCloseAction(byIndexPath: indexPath, isScrollToCurrentSubmessage: true)
            }
            completion()
        }
    }
    
    private func displayShowedMessages(arr1:[ChatEntry], arr2:[ChatEntry]) -> [ChatEntry] {
        var tempArray:[ChatEntry]
        ///Passing parameter isShown from old messages array to new
        for item1 in arr1 {
            if item1.isShown {
                print("********* Old array count = \(arr1.count)")
                print("********* Old item submessages count = \(item1.list.count)")
                for item2 in arr2 {
                    if item1.taskId == item2.taskId, item2.isONE {
                        print("********* New item submessages count = \(item2.list.count)")
                        print("********* New array count = \(arr2.count)")
                        item2.isShown = true
                    }
                }
            }
        }
        
        ///Inserting submessages for showing message in the new array
        tempArray = arr2
        for (index, item) in arr2.enumerated() {
            if item.isShown {
                let submessages = item.list
                tempArray.insert(contentsOf: submessages, at: index + 1)
            }
        }
        print("New array with submessages count = \(tempArray.count)")
        self.hideLoader()
        return tempArray
    }
    
    private func checkTask() {
        self.getScanerResult({ text, type, _  in
            if type.contains("EAN-13") || type.contains("EAN-128") {
                Communicator.checkTask(phone: Session.phone!, code: text) { [unowned self] (result) in
                    if (result["TASK_PRICE"] as! [String: Int])["BONUS"] != 0 {
                        DispatchQueue.main.async {
                            self.showAllertMesssage(title: nil, message: "Продукт: \(result["MODEL"] ?? "") участвует в задании. Вознаграждение составляет: \(result["BONUS"] ?? "") грн.")
                            self.view.layoutIfNeeded()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.showAllertMesssage(title: nil, message: "Продукт не принимает участия в задании")
                            self.view.layoutIfNeeded()
                        }
                    }
                }
            }
        })
    }
    
    //MARK: - Helpers
    private func showAllertMesssage(title: String? = nil, message: String? = nil, isAction: Bool = true, actionStyle: UIAlertAction.Style = .default, searchBar: UISearchBar? = nil) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        if isAction {
            let okAction = UIAlertAction(title: "Ok", style: actionStyle){ _ in
            }
            alert.addAction(okAction)
        }
        if let bar = searchBar{
            alert.view.addSubview(bar)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAllertMesssage(chatMessage: ChatEntry, textAnswer:String, title: String? = nil, message: String? = nil, actionStyle: UIAlertAction.Style = .default) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: actionStyle){ [unowned self] action in
            self.checkIn {
                self.showActionSheet(message: chatMessage, textAnswer: textAnswer)
            }
        }
        alert.addAction(okAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: actionStyle, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
        
    //MARK: - Action button operations
    func showActionSheet(message:ChatEntry, textAnswer:String) {
        var tpId: String = ""
        guard let separatedUrl = message.buttonUrl.components(separatedBy: "?id=") as [String]?,
            let id = separatedUrl.last,
            let url = separatedUrl.first,
            let expected = message.expected,
            let phone = Session.phone else {return}
        let taskType = message.taskType
        if let checkId = Session.checkInText {
            tpId = checkId
            Session.checkInText = nil
        }
        if taskType == TaskType.textImage || taskType == TaskType.image || taskType == TaskType.textImageTp || taskType == TaskType.imageTp{
            imageAction(expected: expected, taskType: taskType, phone: phone, textAnswer: textAnswer, id: id, tpId: tpId)
        } else {
            textAction(taskType: taskType, url: url, phone: phone, textAnswer: textAnswer, id: id, tpId: tpId)
        }
    }
    
    private func textAction(taskType:String, url:String, phone:String, textAnswer:String, id:String, tpId:String) {
        if textAnswer.count != 0 {
            Communicator.textAnswer(url: url, phone: phone, id: id, textAnswer: textAnswer, tpId: tpId) { [unowned self] _ in
                self.chatHistory(withDelay: UInt32(0.5)){}
            }
        } else {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Введите ответ", style: .cancel, handler:{ (UIAlertAction)in
            }))
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
    }
    
    private func imageAction(expected:[Int], taskType:String, phone:String, textAnswer:String, id:String, tpId:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if ((taskType == TaskType.textImage || taskType == TaskType.textImageTp) && textAnswer.count == 0) {
            alert.addAction(UIAlertAction(title: "Введите ответ", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Введите ответ button")
            }))
        } else {
            for i in expected {
                switch i {
                case 0:
                    alert.addAction(UIAlertAction(title: "Камера", style: .default , handler:{ (UIAlertAction)in
                        self.getImage(fromSourceType: .camera) { image in
                            Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, tpId: tpId, completion: { [unowned self] _ in
                                self.chatHistory(withDelay: UInt32(0.5)){}
                            })
                            self.isActionsDone = true
                            self.postNotificationIsActions()
                        }
                    }))
                case 3:
                    alert.addAction(UIAlertAction(title: "Сканировать серийный номер", style: .default , handler:{ (UIAlertAction)in
                        self.getImage(fromSourceType: .camera) { image in
                            Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, tpId: tpId, completion: { [unowned self] _ in
                                self.chatHistory(withDelay: UInt32(0.5)){} })
                            self.isActionsDone = true
                            self.postNotificationIsActions()
                        }
                    }))
                case 2:
                    alert.addAction(UIAlertAction(title: "Галерея", style: .default , handler:{ (UIAlertAction)in
                        self.getImage(fromSourceType: .photoLibrary) { image in
                            Communicator.photoAnswer(phone: phone, photo: image, id: id, textAnswer: textAnswer, tpId: tpId, completion: { [unowned self] _ in
                                self.chatHistory(withDelay: UInt32(0.5)){} })
                            self.isActionsDone = true
                            self.postNotificationIsActions()
                        }
                    }))
                case 1:
                    alert.addAction(UIAlertAction(title: "Сканировать штрихкод/QR-код", style: .default , handler:{ (UIAlertAction)in
                        self.getScanerResult({ text, _, _ in
                            Communicator.photoAnswer(phone: phone, id: id, value: text, textAnswer: textAnswer, tpId: tpId, completion: { [unowned self] _ in
                                self.chatHistory(withDelay: UInt32(0.5)){}
                            })
                            self.isActionsDone = true
                            self.postNotificationIsActions()
                        })
                    }))
                default: break
                    
                }
            }

            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Отмена button")
                self.isActionsDone = false
                self.postNotificationIsActions()
            }))
        }
        
        self.present(alert, animated: true, completion: {
            print("completion block")
            
        })
    }
    
    private func getScanerResult(_ callback: @escaping ((String, String, UIViewController) -> ())) {
        getTextCallback = callback
        self.performSegue(withIdentifier: "scanCode", sender: nil)
    }
    
    private func checkIn(_ callback: @escaping (() -> ())) {
        getScanerResult { (text, type, vc) in
            if let vc = vc as? BarcodeReaderViewController, type.contains("QRCode") {
                Session.checkInText = text
                vc.navigationController?.popViewController(animated: true)
                callback()
            }
        }
        
    }
    
    //MARK: - ImagePickerController
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType, _ callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Transition
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if case let controller as BarcodeReaderViewController = segue.destination,
            segue.identifier == "scanCode" {
            controller.completion = { [unowned self] text, type, vc in
                if let vc = vc {
                    self.getTextCallback?(text, type, vc)
                }
            }
        }
    }
    
   
    //MARK: - Floating Action Button library
    private func setJJFloatingActionButton() {
        let actionButton = JJFloatingActionButton()

        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 6).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        
        actionButton.handleSingleActionDirectly = false
        actionButton.buttonDiameter = 55
        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        actionButton.buttonColor = .newDesignYellow
        actionButton.buttonImageColor = .newDesignBlue
        actionButton.buttonImageSize = CGSize(width: 40, height: 40)
        
        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)

        actionButton.layer.shadowColor = UIColor.black.cgColor
        actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        actionButton.layer.shadowOpacity = Float(0.4)
        actionButton.layer.shadowRadius = CGFloat(2)

        actionButton.itemSizeRatio = CGFloat(0.75)
        actionButton.configureDefaultItem { item in
            item.buttonColor = .white
            item.imageSize = CGSize(width: 42, height: 42)

            item.layer.shadowColor = UIColor.black.cgColor
            item.layer.shadowOffset = CGSize(width: 0, height: 1)
            item.layer.shadowOpacity = Float(0.4)
            item.layer.shadowRadius = CGFloat(2)
        }
        
        ///Message item
        actionButton.addItem(image: UIImage(named: "sendMsg")!) { item in
          // do something
        }
        
        ///Filter item
        actionButton.addItem(image: UIImage(named: "filter")!) { item in
            self.showFilteringMessagesView()
        }
        
        ///Registration place work item
        localeBtn = actionButton.addItem()
        if Session.isCheckin {
            localeBtn.imageView.image = UIImage(named: "registrationByPlace")
        } else {
            localeBtn.imageView.image = UIImage(named: "registrationByPlace")
        }
        localeBtn.action = { item in
            self.setLocale()
        }
        
        ///Scan product barcode item
        actionButton.addItem(image: UIImage(named: "scanBarCode")!) {[unowned self] item in
            self.checkTask()
        }
        
        ///Refresh item
        actionButton.addItem(image: UIImage(named: "update")!) {[unowned self] item in
            self.chatHistory(withDelay: UInt32(0.5)){
                self.tableView.reloadData()
            }
        }
        
        ///Tranzition to bottom table view item
        getBottomBtn = actionButton.addItem()
        getBottomBtn.titleLabel.text = "Owl"
        getBottomBtn.imageView.image = UIImage(named: "ic_arrow_up")
        getBottomBtn.buttonColor = .white
        getBottomBtn.imageSize = CGSize(width: 30, height: 30)
        getBottomBtn.action = { item in
            self.tableView.scrollToTop()
        }
    }
    
    private func setLocale() {
        if !Session.isCheckin {
            self.getScanerResult({ text, type, _  in
                if type.contains("QRCode") {
                    Communicator.checkIn(phone: Session.phone!, qrCode: text, completion: { [unowned self] (result) in
                        if (result["STATUS"] as! String).contains("OK") {
                            DispatchQueue.main.async {
                                //                                self.localeBtn.isUserInteractionEnabled = false
                                self.localeBtn.imageView.image = UIImage(named: "geotag_green")
                                self.view.layoutIfNeeded()
                            }
                            Session.isCheckin = true
                        }
                        self.chatHistory(withDelay: UInt32(0.5)){}
                    })
                }
            })
        }
    }
}

//MARK: - Table view
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.messages.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let setUpCellHelper = SetUpChatCellMain()
        let isONE = messages[indexPath.row].isONE
        switch isONE {
            
        case true:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TextPlusImageTypeViewCell.self), for: indexPath) as! TextPlusImageTypeViewCell
            cell.delegate = self
            let message = messages[indexPath.row]
            if let urlString = message.imageUrl, let imageName = urlString.split(separator: "/").last {
                if let image = UIImage().getSavedImageByName(name: String(imageName)){
                    message.imageLocalName = String(imageName)
                    message.imageLocalCropRatio = image.getCropRatio()
                } else {
                    DispatchQueue.global(qos: .utility).async{
                        if let url = URL(string: urlString), let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                message.imageLocalName = String(imageName)
                                image.saveImageByName(name: message.imageLocalName)
                                message.imageLocalCropRatio = image.getCropRatio()
                            }
                        }
                    }
                }
            }
            return setUpCellHelper.setupCell(cell: cell, message: self.messages[indexPath.row], indexPath: indexPath)
            
        case false:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TextPlusImageSubCell.self), for: indexPath) as! TextPlusImageSubCell
            cell.delegate = self
            cell.message = messages[indexPath.row]
            cell.commentLabel.text = ""
            cell.setupCell(message: self.messages[indexPath.row])
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("++++++++Cell is selected++++++++")
        print("Image name: \(messages[indexPath.row].imageLocalName)")
        print("Image URL: \(String(describing: messages[indexPath.row].imageUrl))")
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.getBottomBtn.isUserInteractionEnabled = indexPath.row == self.messages.count - 1 ? false : true
    }

}

//MARK: - ImagePickerControllerDelegate
extension ChatViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        pickImageCallback?(image)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
}

//MARK: - UISearchBar Delegate
extension ChatViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            selectedMessages = allMessages
        } else {
            searchString = searchText
        let searchMessages = allMessages.filter{($0.text.lowercased().contains(searchText.lowercased()))}
            selectedMessages = searchMessages
        }
        tableView.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchString = ""
        removeSubview(tag: 100)
        messages = allMessages
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

//MARK: - PopUpMenuDelegate
extension ChatViewController: PopUpMenuDelegate {
    func selectTabBarItem(index: Int) {
        tabBarController!.selectedIndex = index
    }
    
    func sendingCurrentViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - TextPlusImageTypeViewCellDelegate
extension ChatViewController: TextPlusImageTypeViewCellDelegate, TextPlusImageSubCellDelegate {
    func avatarBtnClicked(_ cell: TextPlusImageSubCell) {
        if let vc = ViewProfileVC.storyboardInstance() {
            vc.message = cell.message
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func avatarBtnClicked(_ cell: TextPlusImageTypeViewCell) {
        if let vc = ViewProfileVC.storyboardInstance() {
            vc.message = cell.message
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func actionBtnClicked(_ cell: TextPlusImageTypeViewCell, message: ChatEntry, textAnswer: String) {
        let indexPath = cell.selfIndexPath
        Session.indexPath = indexPath
        let taskType = message.taskType
        if taskType == TaskType.checkListTp ||
            taskType == TaskType.imageTp ||
            taskType == TaskType.textImageTp ||
            taskType == TaskType.textTp {
            showAllertMesssage(chatMessage: message, textAnswer: textAnswer, message: "Для выполнения задания нужно произвести регистрацию на рабочем месте")
        } else {
            showActionSheet(message: message, textAnswer: textAnswer)
        }
    }
    
    func openCloseBtnClicked(_ cell: TextPlusImageTypeViewCell) {
        let indexPath = cell.selfIndexPath
        print("***** openCloseBtn indexPath = \(indexPath)")
        Session.indexPath = indexPath
        openCloseAction(byIndexPath: indexPath)
    }
    
    func rewardBtnClicked(_ cell: TextPlusImageTypeViewCell, message:ChatEntry) {
        if let userId = Session.phone {
            let webView = WebViewController(stringUrl: APIConfigs.rewardsLink, userId: userId, taskId: String(message.taskId))
            self.present(webView, animated: true, completion: nil)
        }
    }
    
    private func openCloseAction(byIndexPath:IndexPath, isScrollToCurrentSubmessage:Bool = false) {
        var index: Int = byIndexPath.row
        var tempIndexPath = byIndexPath
        var temp1Array = [ChatEntry]()
        var temp2Array = [ChatEntry]()
        print("\n********************************************************************\n")
        print("*** Метод openCloseAction был вызван поиндексу = \(index)")
        print("*** Входящий массив (messages) содержит = \(messages.count) записей")
        
        var times = 0
        for (i, item) in messages.enumerated() {
            ///Set up correct index If selected cell has a bigger index then previous
            let subtrahend = isScrollToCurrentSubmessage ? item.list.count - 1 : item.list.count
            if item.isShown, i < index {
                index = (index - subtrahend)
                tempIndexPath = IndexPath(row: index, section: 0)
                print("*** Коррекция входящего индекса = \(index) , and list count = \(item.list.count)")
                break
            }
            times += 1
        }
        print("Кол-во итераций при коррекции индекса = \(times)")
        
        for item in messages {
            ///Clear array from submessages
            if item.isONE {
                temp1Array.append(item)
//                print("*** isShown in messages = \(item.isShown)")
            }
        }
        print("************* temp1Array count after cleaning = \(temp1Array.count)")
        
        for (i, item) in temp1Array.enumerated() {
            
            ///Set up isShown bool value for all items
            if i == index {
                item.isShown = !item.isShown
            } else {
                item.isShown = false
            }
            print("********** i = \(i)\n*********** index = \(index)\n*********** = isSown = \(item.isShown)")
            ///Formation a new array
            temp2Array.append(item)
            if item.isShown, i == index {
                temp2Array.append(contentsOf: item.list)
                print("************* Submessages added = \(item.isShown) в кол-ве = \(item.list.count)")
            }
        }
        print("************* temp2Array count = \(temp2Array.count)")
        
        ///Sending values to tableView
        combinedMessages = temp2Array
        tableView.reloadData()
        
        ///Scrolling submessages after task execution
//        if messages[tempIndexPath.row].isShown,
//            isScrollToCurrentSubmessage {
//            let row = tempIndexPath.row + messages[tempIndexPath.row].list.count
//            let combIndexPath = IndexPath(row: row, section: 0)
//            self.tableView.scrollToCurrent(combIndexPath) {self.tableView.reloadData()}
//        } else {
//            self.tableView.scrollToCurrent(tempIndexPath) {self.tableView.reloadData()}
//        }
        
    }
    
}
