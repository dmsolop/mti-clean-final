//
//  SetUpChatCellMain.swift
//  Loyality
//
//  Created by Димон on 29.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit
import LTHRadioButton
import PureLayout

class SetUpChatCellMain: NSObject {
    
//    private enum TaskType: String {
//        case checkList = "CHECK_LIST"
//        case text = "TEXT"
//        case image = "IMAGE"
//        case textImage = "TEXT_IMAGE"
//    }
    
    enum StatusImage: String {
        case cross =       "red_cross"
        case checkmark =   "green_checkmark"
        case uah =         "yelow_uah"
    }
    
    enum StatusText: String {
        case sended =       "Отправлено"
        case accepted =     "Принято"
        case paid =         "Оплачено"
        case rejected =     "Отклонено"
        case rearranged =   "Перевыставлено"
        case overdue =      "Просрочено"
        case doneByOthers = "Выполнено другими"
        case fraud =        "Мошенничество"
    }
    
    var thisCell: TextPlusImageTypeViewCell!
    var questions = [String]()
    
    var imageWidth: CGFloat?
    var imageCrop: CGFloat?
    var isText: Bool = false
    var textAnswer: String = ""
    
    var message: ChatEntry!
    var subMessages: [ChatEntry]!
    var isShowSubcells = false
    
    
    //MARK: - Set up cell method
    func setupCell(cell: TextPlusImageTypeViewCell, message: ChatEntry, indexPath: IndexPath) -> TextPlusImageTypeViewCell{
        cell.mainImage.image = UIImage()
        cell.actionBtnTitle.text = ""
        cell.textField.text = ""
        cell.mainTextView.font = .systemFont(ofSize: CGFloat(Constants.chatTextFontSize))
        self.questions = [String]()
        for view in cell.checkboxStackview.arrangedSubviews {
            view.removeFromSuperview()
        }
        if Session.indexPath != indexPath {
            cell.openCloseList.setImage(UIImage(named: "arrow_down"), for: .normal)
            self.isShowSubcells = false
        } else {
            cell.openCloseList.setImage(UIImage(named: "arrow_up"), for: .normal)
            self.isShowSubcells = true
        }
        
        self.thisCell = cell
        self.message = message
        cell.message = message
        cell.rewardBtn.isUserInteractionEnabled = message.taskCost == -1 ? true : false
        
        //MARK: - Set up checkbox view
        
        if let quess = message.questions?.components(separatedBy: "###"), message.status == 0 {
            for item in quess {
                if item != "" {
                    self.questions.append(item)
                }
            }
        }
        
        if self.questions.count != 0 {
            cell.heightCheckbox.constant = CGFloat(self.questions.count * 45)
            cell.bottomCheckbox.constant = 8
            cell.setupCheckboxView(questions: questions)
        } else {
            cell.heightCheckbox.constant = 0
            cell.bottomCheckbox.constant = 0
        }
        
        //MARK: - Set up reward label
        let textRewardLabel = message.taskCost != -1 ? "  Вознаграждение: " : "  Вознаграждение "
        let taskCostType = message.taskCoastType == 1 ? "%  " : "  "
        let taskCost = message.taskCost == -1 ? "" : String(message.taskCost)
        cell.rewardLabel.layer.cornerRadius = cell.heightRewardLabel.constant / 2
        cell.rewardLabel.text = textRewardLabel + taskCost + taskCostType
        cell.rewardLabel.backgroundColor = UIColor.newDesignYellow
        
        //MARK: - Set up openCloseButton
        subMessages = message.list
        if subMessages.count > 0 {
            cell.openCloseList.isHidden = false
            cell.heightOpenCloseBtn.constant = 44
            cell.bottomOpenCloseBtn.constant = 8
            let nameImageBtn = message.isShown ? "arrow_up" : "arrow_down"
            cell.openCloseList.setImage(UIImage(named: nameImageBtn), for: .normal)
        } else {
            cell.openCloseList.isHidden = true
            cell.heightOpenCloseBtn.constant = 0
            cell.bottomOpenCloseBtn.constant = 0
        }
        
        
        //MARK: - Set up avatars
        if let urlString = message.avatar {
            cell.avatarLeft.sd_setImage(with: URL(string: urlString), completed: nil)
        } else {
            cell.avatarLeft.image = UIImage.init(named: "man_white")
        }
        cell.avatarRight.isHidden = true
        cell.avatarLeft.cornerRadius = cell.avatarLeft.layer.frame.height/2
        cell.avatarRight.cornerRadius = cell.avatarLeft.layer.frame.height/2
        
        //MARK: - Set up main image
        if let image = UIImage().getSavedImageByName(name: message.imageLocalName) {
            let width = cell.frame.size.width
            cell.mainImage.image = image
            cell.heightMainImage.constant = width / message.imageLocalCropRatio
            cell.mainImage.isHidden = false
            cell.bottomMainImage.constant = 8
        } else {
            cell.mainImage.isHidden = true
            cell.heightMainImage.constant = 0
            cell.bottomMainImage.constant = 0
        }
        
        
        
        /*cell.mainImage.sd_setImage(with: URL(string: message.imageUrl ?? "")) { (image, error, casheImage, url) in
            DispatchQueue.main.async {
                if let err = error {
                    print(err)
                    cell.mainImage.isHidden = true
                    cell.heightMainImage.constant = 0
                    cell.bottomMainImage.constant = 0
                } else {
                    let width = cell.frame.size.width
                    cell.mainImage.image = image
                    cell.heightMainImage.constant = width / message.imageLocalCropRatio
                    cell.mainImage.isHidden = false
                    cell.bottomMainImage.constant = 8
                }
            }
        }*/
        
        //MARK: - Set up textField
        switch message.taskType {
        case TaskType.text,
             TaskType.textImage,
             TaskType.textTp,
             TaskType.textImageTp:
            cell.textField.isHidden = false
            cell.textField.layer.cornerRadius = cell.textField.layer.frame.height/2
            cell.heightTextField.constant = 44
            cell.bottomTextField.constant = 8
        default:
            cell.textField.isHidden = true
            cell.heightTextField.constant = 0
            cell.bottomTextField.constant = 0
        }
        
        //MARK: - Set up main text view
        
        if message.text.first == "<" {
            cell.mainTextView.attributedText = message.text.convertHtml()
        } else {
            cell.mainTextView.text = message.text
        }
        
        //MARK: - Set up state label and image
        if message.status != 0 {
            cell.stateLabel.isHidden = false
            cell.heightStateLabel.constant = 40
            cell.bottomStateLabel.constant = 8
            
            switch message.status {
            case 0:
                cell.stateLabel.text = StatusText.sended.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 1:
                cell.stateLabel.text = StatusText.accepted.rawValue
                cell.imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
            case 2:
                cell.stateLabel.text = StatusText.paid.rawValue
                cell.imageState.image = UIImage(named: StatusImage.uah.rawValue)
            case 3:
                cell.stateLabel.text = StatusText.rejected.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 4:
                cell.stateLabel.text = StatusText.rearranged.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 5:
                cell.stateLabel.text = StatusText.overdue.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 6:
                cell.stateLabel.text = StatusText.doneByOthers.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            case 7:
                cell.stateLabel.text = StatusText.fraud.rawValue
                cell.imageState.image = UIImage(named: StatusImage.cross.rawValue)
            default: break
            }
        } else {
            cell.stateLabel.isHidden = true
            cell.heightStateLabel.constant = 0
            cell.bottomStateLabel.constant = 0
        }
        
        //MARK: - Set up action button
        if let bText = message.buttonText, message.status == 0 {
            cell.actionBtn.isHidden = false
            cell.actionBtnTitle.isHidden = false
            cell.actionBtnTitle.text = bText
            cell.actionBtnTitle.cornerRadius = cell.actionBtn.layer.frame.height/2
            cell.heightActionBtn.constant = 44
            cell.bottomActionBtn.constant = 8
            cell.layoutIfNeeded()
        } else {
            cell.actionBtn.isHidden = true
            cell.actionBtnTitle.isHidden = true
            cell.heightActionBtn.constant = 0
            cell.bottomActionBtn.constant = 0
        }
        
        //MARK: - Set up date label
        cell.dateLabel.text = message.dateAndTime
        
        return cell
    }
    
//    func showCheckmarkAfterTask(cell: TextPlusImageTypeViewCell) {
//        if isActionsDone {
//            cell.stateLabel.isHidden = false
//            cell.heightStateLabel.constant = 40
//            cell.bottomStateLabel.constant = 8
//
//            cell.actionBtn.isHidden = true
//            cell.actionBtnTitle.isHidden = true
//            cell.heightActionBtn.constant = 0
//            cell.bottomActionBtn.constant = 0
//
//            cell.stateLabel.text = StatusText.sended.rawValue
//            cell.imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
//        }
//    }
    
}

extension SetUpChatCellMain: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.textAnswer = textField.text! + string
        
        return true
    }
    
}

extension SetUpChatCellMain: UITextViewDelegate {
    
}


