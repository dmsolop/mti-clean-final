//
//  TextPlusImageTypeViewCell.swift
//  Loyality
//
//  Created by Dima Solop on 7/11/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import LTHRadioButton
import PureLayout

protocol TextPlusImageTypeViewCellDelegate: class {
    func actionBtnClicked(_ cell:TextPlusImageTypeViewCell, message:ChatEntry, textAnswer:String)
    func openCloseBtnClicked(_ cell:TextPlusImageTypeViewCell)
    func rewardBtnClicked(_ cell:TextPlusImageTypeViewCell, message:ChatEntry)
    func avatarBtnClicked(_ cell:TextPlusImageTypeViewCell)
}

class TextPlusImageTypeViewCell: UITableViewCell, UITextViewDelegate {
    
    enum StatusImage: String {
        case cross =       "red_cross"
        case checkmark =   "green_checkmark"
        case uah =         "yelow_uah"
    }

    enum StatusText: String {
        case sended =       "Отправлено"
        case accepted =     "Принято"
        case paid =         "Оплачено"
        case rejected =     "Отклонено"
        case rearranged =   "Перевыставлено"
        case overdue =      "Просрочено"
        case doneByOthers = "Выполнено другими"
        case fraud =        "Мошенничество"
    }
    
    @IBOutlet weak var avatarLeft: UIImageView!
    @IBOutlet weak var avatarRight: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var heightTextField: NSLayoutConstraint!
    @IBOutlet weak var bottomTextField: NSLayoutConstraint!
    
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var heightStateLabel: NSLayoutConstraint!
    @IBOutlet weak var imageState: UIImageView!
    @IBOutlet weak var bottomStateLabel: NSLayoutConstraint!
    
    @IBOutlet weak var actionBtnTitle: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var heightActionBtn: NSLayoutConstraint!
    @IBOutlet weak var bottomActionBtn: NSLayoutConstraint!
    
    @IBOutlet weak var openCloseList: UIButton!
    @IBOutlet weak var heightOpenCloseBtn: NSLayoutConstraint!
    @IBOutlet weak var bottomOpenCloseBtn: NSLayoutConstraint!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var heightMainImage: NSLayoutConstraint!
    @IBOutlet weak var bottomMainImage: NSLayoutConstraint!
    
    @IBOutlet weak var checkboxStackview: UIStackView!
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var bottomCheckbox: NSLayoutConstraint!
    @IBOutlet weak var heightCheckbox: NSLayoutConstraint!
    
    @IBOutlet weak var mainTextView: UITextView!
    
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var rewardBtn: UIButton!
    @IBOutlet weak var heightRewardLabel: NSLayoutConstraint!
    
    var delegate: TextPlusImageTypeViewCellDelegate?
    var radioButtons = [LTHRadioButton]()
    var imageWidth: CGFloat?
    var imageCrop: CGFloat?
    var selfIndexPath: IndexPath {
        return getSelfIndexPath()
    }
    var textAnswer: String = ""
    private var isActionsDone = false {
        didSet {
            showCheckmarkAfterTask()
        }
    }

    var message: ChatEntry!
    var subMessages: [ChatEntry]!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        addNotificationCenter()
        imageWidth = mainImage.frame.width
        textField.delegate = self
        mainTextView.delegate = self
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        mainImage.image = nil
//    }
    
    //MARK: Actions
    
    @IBAction func actionBtnClicked(_ sender: UIButton) {
        delegate?.actionBtnClicked(self, message: message, textAnswer: textAnswer)
        self.textField.text = ""
    }
    
    @IBAction func openCloseBtnClicked(_ sender: UIButton) {
        delegate?.openCloseBtnClicked(self)
    }
    
    @IBAction func rewardBtnClicked(_ sender: UIButton) {
        delegate?.rewardBtnClicked(self, message:message)
    }
    
    @IBAction func avatarsClicked(_ sender: UIButton) {
        delegate?.avatarBtnClicked(self)
    }
    
    
    //MARK: - Notifications
    private func addNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(setIsActionsDone(_:)),
                                       name: .isActionsDone,
                                       object: nil)

        
    }

    private func getSelfIndexPath() -> IndexPath {
        var myIndexPath: IndexPath?
        self.indexPath.flatMap { myIndexPath = $0 }

        return myIndexPath ?? IndexPath()
    }
    
    @objc private func setIsActionsDone(_ notification: NSNotification) {
        guard let dict = notification.userInfo as NSDictionary?,
            let isActionClicked = dict["isActions"] as? Bool else { return }
        isActionsDone = isActionClicked
    }
    
    func showCheckmarkAfterTask() {
        if isActionsDone {
            stateLabel.isHidden = false
            heightStateLabel.constant = 40
            bottomStateLabel.constant = 8

            actionBtn.isHidden = true
            actionBtnTitle.isHidden = true
            heightActionBtn.constant = 0
            bottomActionBtn.constant = 0

            stateLabel.text = StatusText.sended.rawValue
            imageState.image = UIImage(named: StatusImage.checkmark.rawValue)
        }
    }
    
    //MARK: - CheckBox
        @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
            if let sender = sender {
                for btn in radioButtons {
                    if btn.tag == sender.view?.tag {
                        btn.select(animated: true)
                        if let subviews = sender.view?.subviews {
                            for item in subviews {
                                if item .isKind(of: UILabel.self) {
                                    self.textAnswer = (item as! UILabel).text!
                                }
                            }
                        }
                        
                    } else {
                        btn.deselect(animated: true)
                    }
                }
            }
        }
        
    func setupCheckboxView(questions: [String]) {
            
            let widthForLabel = checkboxStackview.frame.width

            for (index, item) in questions.enumerated() {
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                tapGesture.delegate = self as UIGestureRecognizerDelegate
                let radioButton = LTHRadioButton(diameter: 20, selectedColor: UIColor.gray, deselectedColor: UIColor.gray)
                let label = UILabel()
                let view1 = UIView()
                
                view1.backgroundColor = UIColor.clear
                
                label.text = String(item)
                label.textAlignment = .left
                label.textColor = UIColor.gray
                label.font = UIFont(name: "Helvetica Neue", size: 15)
                label.numberOfLines = 2
                label.lineBreakMode = .byWordWrapping
                
                label.tag = index+1
                view1.tag = index+1
                radioButton.tag = index+1
                
                radioButtons.append(radioButton)
                
                view1.addSubview(radioButton)
                view1.addSubview(label)
                view1.addGestureRecognizer(tapGesture)
                view1.isUserInteractionEnabled = true
                
                radioButton.autoPinEdge(toSuperviewEdge: .left, withInset: 4)
                radioButton.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
    //            radioButton.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
                
                label.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
                label.autoPinEdge(toSuperviewEdge: .right)
                label.autoSetDimension(ALDimension.width, toSize: widthForLabel - 30)

                checkboxStackview.addArrangedSubview(view1)
            }
        }

}

extension TextPlusImageTypeViewCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.textAnswer = textField.text! + string
        return true
    }

}

