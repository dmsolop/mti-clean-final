//
//  CardCollectionViewCell.swift
//  Loyality
//
//  Created by Denis Romashov on 4/9/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import EAN13BarcodeGenerator

class CardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var barcodeView: BarCodeView!
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var barCodeNumbers: UILabel!
    //    @IBOutlet weak var barcodeImageView: UIImageView!
    
}
