//
//  StringEx.swift
//  Loyality
//
//  Created by Denis Romashov on 3/19/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

extension String {
    func checkToPattern(pattern: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            if let _ = regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.utf16.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        return false
    }
}

extension String {
    func aesEncrypt(_ key: String, iv: String) throws -> String{
        let md5Key = Security.md5(string: key)
        let data = self.data(using: String.Encoding.utf8)

        
        let enc = try AES(key: md5Key, blockMode: ECB(), padding: .pkcs7).encrypt(data!.bytes)
        
        let encData = Data(bytes: UnsafePointer<UInt8>(enc), count: Int(enc.count))
        let base64String: String = encData.base64EncodedString(options:
            NSData.Base64EncodingOptions(rawValue: 0));
        let result = String(base64String)
        
        
        return result
    }
    
//    func aesDecrypt(_ key: String, iv: String) throws -> String {
//        let data = Data(base64Encoded: self, options:NSData.Base64DecodingOptions(rawValue: 0))
//        let dec = try AES(key: key, iv: iv, blockMode:ECB()).decrypt(data!.bytes)
////        let decData = Data(bytes: UnsafePointer<UInt8>(dec), count: Int(dec.count))
//        let decData = Data(bytes:dec, count: Int(dec.count))
//        let result = NSString(data: decData, encoding: String.Encoding.utf8.rawValue)
//        return String(result!)
//    }
}

extension String {
    
    /// Convert HTML to NSAttributedString
       func convertHtml() -> NSAttributedString {
           guard let data = data(using: .utf8) else { return NSAttributedString() }
           if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil) {
               let string = NSMutableAttributedString(attributedString: attributedString)

               // Apply text color
               string.addAttributes([.foregroundColor: UIColor.black], range: NSRange(location: 0, length: attributedString.length))

               // Update fonts
               let regularFont = UIFont(name: "HelveticaNeue", size: 17)! // DEFAULT FONT (REGUALR)
               let boldFont = UIFont(name: "HelveticaNeue-Bold", size: 17)! // BOLD FONT
               let italicFont = UIFont(name: "HelveticaNeue-Italic", size: 17)! // ITALIC FONT

               string.enumerateAttribute(.font, in: NSMakeRange(0, attributedString.length), options: NSAttributedString.EnumerationOptions(rawValue: 0), using: { (value, range, stop) -> Void in

                   /// Update to our font
                   // Bold font
                   if let oldFont = value as? UIFont, oldFont.fontName.lowercased().contains("bold") {
                       string.removeAttribute(.font, range: range)
                       string.addAttribute(.font, value: boldFont, range: range)
                   } // Courier font
                   else if let oldFont = value as? UIFont, oldFont.fontName.lowercased().contains("italic") {
                       string.removeAttribute(.font, range: range)
                       string.addAttribute(.font, value: italicFont, range: range)
                   }
                   // Default font
                   else {
                       string.addAttribute(.font, value: regularFont, range: range)
                   }
               })
               return string
           }
           return NSAttributedString()
       }
}
